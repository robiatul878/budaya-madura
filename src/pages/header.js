import React, { Component } from 'react'
import {Text, StyleSheet, View, TouchableOpacity } from 'react-native'


export default class header extends Component {
  render() {
    return (
      <View>
       <Text style={{fontSize:20,fontWeight:'bold',color:'brown', textAlign:'center',}}>PAREBASAN BASA MADURA</Text>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  header: {
    paddingHorizontal: 30,
    paddingTop: 20,
    
  },
  headerText: {
    fontSize: 20,
    fontWeight: 'bold',
  },
  underLine: {
    borderWidth: 1,

    marginTop: 10,
  },
});
