import React, {useEffect} from 'react';
import {Image, StyleSheet, Text, View} from 'react-native';


const Splash =({navigation}) => {
    useEffect(() => {
     setTimeout (() => {
         navigation.replace('header');
     },5000);
});
return (
    <View style={styles.wrapper}>
        <Image source={require('../assets/splash.jpg')} style={{height:100,width:100}}/>
    </View>     
    );
};

export default Splash;

const styles = StyleSheet.create({
    wrapper: {
        backgroundColor:'#F5DEB3',
        alignItems: 'center',
        justifyContent: 'center',
        flex :2,

    },
    logo:{
        margin: 10,
    },
});
